This module is a fork of the Image Block module with adaptions made to 
account for the use of the Picture element.

The admin user is presented with two separate image upload fields for 
desktop and mobile usage and an additional field to allow for the setting 
of a pixel width to activate a min-width media query.

How to use:
1. Enable Picture block module.</li>
2. Navigate to admin/build/block.</li>
3. Click the "Add Picture block" link in the local tasks menu.</li>
4. Configure your Picture block, and save.
