<?php

/**
 * @file
 * Default theme implementation to display Picture block content.
 *
 * Available variables:
 * - $image: Block image.
 * - $content: Block content.
 * - $block: Block object.
 *
 * @see template_preprocess()
 * @see template_preprocess_pictureblock_content()
 */
?>
<?php if ($image): ?>
  <div class="block-image">
    <a href="<?php print $linkpath; ?>" title="<?php print $title; ?>"><picture>
        <source srcset="<?php print $dpath ?>" media="(min-width: <?php print $breakpoint; ?>px)">
        <img srcset="<?php print $mpath; ?>" alt="<?php print $alt; ?>">
    </picture></a>
  </div>
<?php endif; ?>

<?php if ($content): ?>
  <div class="block-body">
    <?php print $content ?>
  </div>
<?php endif; ?>
